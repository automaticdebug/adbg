#!/usr/bin/env python
# -*- coding: utf-8 -*-

from argparse import (ArgumentParser)
# from AutomaticDebugger import (DivBy0, Duplicate)
from AutomaticDebugger.DivBy0 import (get_sub_parser as DivBy0_subparser)
from AutomaticDebugger.Duplicate import (get_sub_parser as Duplicate_subparser)
from AutomaticDebugger.CFI import (get_sub_parser as CFI_subparser)
from AutomaticDebugger.Unreach import (get_sub_parser as Unreach_subparser)
from AutomaticDebugger.VarEvolution import (get_sub_parser as VarEvolution_subparser)

def command_line():
    cl_parser = ArgumentParser(prog='automatic-debugger')
    sub = cl_parser.add_subparsers(title="Analysers",
                                   description="All our automatic debugger Analysers",
                                   help="hey")
    sub.required = True
    sub.dest = 'analyser'
    DivBy0_subparser(sub)
    Duplicate_subparser(sub)
    CFI_subparser(sub)
    Unreach_subparser(sub)
    VarEvolution_subparser(sub)
    return cl_parser

def main():
    args = command_line().parse_args()
    args.run(args)

if __name__ == "__main__":
    main()
