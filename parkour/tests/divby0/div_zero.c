/*
Decl(_name = 'main', 
     _ctype = FuncType(_params = [], 
		       _decltype = None, 
		       _identifier = 'int', 
		       _specifier = 0, 
		       _storage = 0), 
     body = BlockStmt(body = 
*/
int main()
{
  int i;
  /*
  [Decl(_name = 'i', 
	_ctype = PrimaryType(_decltype = None, 
			     _identifier = 'int', 
			     _specifier = 0, 
			     _storage = 0)), 
  */

  i = 0;
  /*
  ExprStmt(expr = Binary(call_expr = Raw(value = '='), 
			 params = [Id(value = 'i'), 
				   Literal(value = '0')])), 
  */
  1 / 2;
  1 / 0;

  5*(0+2);
  i = 0 + 2;
  /*
  ExprStmt(expr = Binary(call_expr = Raw(value = '='), 
			 params = [Id(value = 'i'),
				   Binary(call_expr = Raw(value = '+'),
					  params = [Literal(value = '0'),
						    Literal(value = '2')])]))
 */

  return (10/i);
  /*
  Return(expr = Paren(call_expr = '()', 
		      params = [Binary(call_expr = Raw(value = '/'), 
				       params = [Literal(value = '10'), 
						 Id(value = 'i')])]), 
	 value = 'return')], 
  */
}
/*
  types = ChainMap({}, 
		   {})))
*/
