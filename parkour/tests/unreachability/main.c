/*
*
*
* FUNCTION POINTER
*
*/

/** USED **/

// int addInt(int n, int m) {
//     return n+m;
// }

/** NOT _ USED **/
// int mulInt(int n, int m) {
//     return n*m;
// }

/*******/


/*
*
* code NEVER USE
*
*/

int never_used_code(int b)
{
  int tmp;

  tmp = b + 2;
  return (tmp);
}


/*******/

/*
*
* code USE by call in another func - so NEVER USE
*
*/

int code_used_by_part_two(int b)
{
  return (b+4);
}

int part_two(int a)
{
  int b;

  b = code_used_by_part_two(a);
  return (b);
}
/*******/

/*
*
* USED CODE IN WHILE
*
*/

int code_in_while()
{
  return (42);
}

/*******/

/*
*
* USED CODE the condition function
*
*/

int simple_return()
{
  return (42);
}
/*
*
* USED CODE IN IF
*
*/

int code_in_condition()
{
  int a = simple_return();

  return (a);
}

/*******/

/*
*
* SIMPLE USED CODE
*
*/

int use_in_used_func()
{
  return (1);
}

int used_code_from(int a)
{
  return (a+4);
}

int used_code(int a)
{


  // not use
  //int (*functionPtr2)(int,int);
  //functionPtr2 = &mulInt;
  // int mul = (*functionPtr2)(2, 3);

  //int test = code_in_while();
  int a = 0;
  if (1 < code_in_condition())
  {
    a = 2;
  }
  int i = 0;
  while (i < code_in_while())
    {
      i++;
    }
  return (a+use_in_used_func());
}

int	main(void)
{
	int	a;
  int y:

  a = 0;
  y = 1;
  y = used_code_from(a);
	a = used_code(a) + 1;
}
