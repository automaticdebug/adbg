
int campingcar = 42;

int toto(int a, int b) {
	return (b / a) * (a - b);
}

int tata(int car, int ri) {
	return (ri / car) * (ri - car);
}

int titi(int ko, int sa) {
	return (sa / ko) * (ko - sa);
}

int tutu(int naval, int car) {
	int a;
	return (car / naval) * (car - naval);
}

int tete(int naval) {
	return (campingcar / naval) * (campingcar - naval); // should not match with tata and tutu but with tyty
}

int tyty(int cu) {
	return (campingcar / cu) * (campingcar - cu); // should not match with tata and tutu but with tete
}
