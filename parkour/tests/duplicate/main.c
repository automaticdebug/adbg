
#include <stdio.h>

int test_1(int karamba, int b) /* is duplicate */
{
	return (karamba * b) / (karamba + b);
}

int test_2(int c, int d) /* is duplicate */
{
	return (c * d) / (c + d);
}

int test_3(int a, int b) /* is not duplicate */
{
	return (a + b) / (a * b);
}

int test_4(int a, int b) /* is not duplicate ? */
{
	test_1(a, b);
	return (a * b) / (a + b);
}

int test_5(int a, int b) /* is duplicate ? */
{
	return (a + 2) / (a * b);
}

int test_6(int a, int b) /* is duplicate ? */
{
	return (a + 42) / (21 * b);
}

int test_7(int a, int b) /* is not duplicate */
{
	if (a > b)
	{
		return (a + b) / (a * b);
	}
	return (a * b) / (a + b);
}

int main(void)
{
	printf("hello world\n");
	return (0);
}

