
int toto(int a) {
	return a * 2;
}

int titi(int b) {
	return b * 3;
}

int tete() {
	return toto(42);
}

int tutu() {
	return titi(42);
}
